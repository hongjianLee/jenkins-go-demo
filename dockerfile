# 构建阶段
FROM golang:1.19 as builder

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GOPROXY=https://goproxy.cn,direct

# 设置工作目录
WORKDIR /app

# 复制文件
COPY . .

# 下载依赖
RUN go mod download

# 编译程序
RUN go build -o app .

# 运行阶段
FROM alpine:3.12

# 复制程序
COPY --from=builder /app/app /app

COPY --from=builder /app/config/config.yml /config/config.yml

#COPY --from=builder /app/templates /templates

ENV PORT=8080

EXPOSE 8080

# 运行程序
ENTRYPOINT ["./app"]