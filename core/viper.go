package core

import (
	"fmt"
	"github.com/spf13/viper"
	"go_base/global"
)

func InitViper() {
	// 初始化Viper框架
	v := viper.New()
	v.SetConfigName("config")
	v.SetConfigType("yml")
	v.AddConfigPath("./config")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s \n", err))
	}
	global.LHJ_VP = v
}
