package core

import (
	"context"
	"github.com/go-redis/redis/v8"
	"go_base/global"
	"log"
)

func InitRedis() {
	client := redis.NewClient(&redis.Options{
		Addr: "101.43.136.107:32769",
		//Password: Password, // no password set
		DB: 0, // use default DB
	})
	pong, err := client.Ping(context.Background()).Result()
	if err != nil {
		log.Println("redis connect ping failed, err:", err)
	} else {
		log.Println("redis connect ping response:", pong)
		global.LHJ_REDIS = client
	}
}
