package api

import (
	"context"
	"crypto/sha1"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/gin-gonic/gin"
	"go_base/global"
	"go_base/model/request"
	"go_base/model/response"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"
)

func HandTest(c *gin.Context) {
	bodyBytes, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.String(http.StatusBadRequest, "Bad request")
		return
	}
	var reqBody RequestBody
	if err := json.Unmarshal(bodyBytes, &reqBody); err != nil {
		c.String(http.StatusBadRequest, "Bad request")
		return
	}
	log.Println(reqBody)
	var name string
	if reqBody.Name == "" {
		name = "hongjian.li@gdscn.ey.com"
	} else {
		name = reqBody.Name
	}
	data := make(map[string]string)
	data["sender"] = name
	result := make(map[string]interface{})
	result["code"] = 200
	result["success"] = true
	result["result"] = data
	c.JSON(http.StatusOK, result)
}

type RequestBody struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

func HandleWechatTest(c *gin.Context) {
	timestamp := c.Query("timestamp")
	nonce := c.Query("nonce")
	signature := c.Query("signature")

	// 获取微信公众号提供的token
	token := global.LHJ_VP.GetString("wechat.token")

	tmpArr := []string{token, timestamp, nonce}
	sort.Strings(tmpArr)
	tmpStr := sha1.Sum([]byte(strings.Join(tmpArr, "")))

	// 将加密结果与微信公众号提供的token进行比较
	if fmt.Sprintf("%x", tmpStr) == signature {
		c.String(http.StatusOK, c.Query("echostr"))
	} else {
		c.String(http.StatusBadRequest, "Invalid signature")
	}
}

func HandleWechatRequest(c *gin.Context) {
	// 读取微信服务器发送过来的消息
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read message error:", err)
		return
	}

	// 解析消息
	var msg request.Message
	err = xml.Unmarshal(body, &msg)
	if err != nil {
		log.Println("Parse message error:", err)
		return
	}
	log.Println(msg.FromUserName)
	global.LHJ_REDIS.HSet(context.Background(), "WECHAT:OPENID", msg.FromUserName)

	// 构造回复消息
	resp := response.Response{
		ToUserName:   msg.FromUserName,
		FromUserName: msg.ToUserName,
		CreateTime:   msg.CreateTime,
		MsgType:      "text",
		Content:      msg.Content,
	}

	// 把回复消息转换为XML格式
	respXML, err := xml.Marshal(resp)
	if err != nil {
		log.Println("Marshal response error:", err)
		return
	}

	// 把XML格式的回复消息发送给微信服务器
	c.Writer.Header().Set("Content-Type", "application/xml")
	c.Writer.WriteString(xml.Header)
	c.Writer.WriteString(string(respXML))
}
