package global

import (
	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
)

var (
	LHJ_VP    *viper.Viper
	LHJ_REDIS *redis.Client
)
