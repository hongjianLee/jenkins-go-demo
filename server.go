package main

import (
	"github.com/gin-gonic/gin"
	"go_base/api"
	"go_base/core"
)

//go:generate go mod init gproject

//go:generate go get github.com/spf13/viper
//go:generate go get github.com/gin-gonic/gin

//go:generate go env -w GO111MODULE=on
//go:generate go env -w GOPROXY=https://goproxy.cn,direct
//go:generate go mod tidy
//go:generate go mod download

func main() {
	core.InitViper()
	core.InitRedis()
	r := gin.Default()
	group := r.Group("wx")
	group.POST("test", api.HandTest)
	// 注册微信公众号token校验接口
	group.GET("wechat", api.HandleWechatTest)
	group.POST("wechat", api.HandleWechatRequest)
	// 启动Web服务器
	r.Run(":8080")
}
