#!/bin/bash

container_name="go-demo"

version="1.0.0"

image_path="registry.cn-hangzhou.aliyuncs.com/lihongjian/"

tag="${image_path}${container_name}:${version}"

username="772147079@qq.com"

echo "版本号：" $tag

echo "----------Stop container----------"

docker stop $container_name

echo "----------Remove container----------"

docker rm -f $container_name

echo "----------Remove image----------"

docker rmi $tag

echo password.txt | docker login --username=$username registry.cn-hangzhou.aliyuncs.com --password-stdin

echo "----------Pull image----------"

docker pull $tag

echo "----------Start container----------"

docker run -itd --name=go-demo -p 8080:8080 $tag
